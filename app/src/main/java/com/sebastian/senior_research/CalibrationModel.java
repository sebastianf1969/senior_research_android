package com.sebastian.senior_research;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

import org.opencv.core.*;
import org.opencv.imgproc.CLAHE;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

public class CalibrationModel {
    private static final String TAG = "MODEL";
    //Max threshold value for canny edge detector. This value affects circle detection.
    private static final int CANNY_THRESHOLD = 100;

    //Max threshold value for accumulator threshold. This value affects circle detection.
    private static final int MAX_ACCUMULATOR_THRESHOLD = 200;

    private Mat frame;
    private CascadeClassifier faceDetector, eyeDetector;

    public CalibrationModel(Context context){
        try {
            // Copy the resource into a temp file so OpenCV can load it
            InputStream is = context.getResources().openRawResource(R.raw.lbpcascade_frontalface);
            File cascadeDir = context.getDir("cascade", Context.MODE_PRIVATE);
            File mCascadeFile = new File(cascadeDir, "lbpcascade_frontalface.xml");
            FileOutputStream os = new FileOutputStream(mCascadeFile);


            byte[] buffer = new byte[4096];
            int bytesRead;
            while ((bytesRead = is.read(buffer)) != -1) {
                os.write(buffer, 0, bytesRead);
            }
            is.close();
            os.close();


            // Load the cascade classifier
            faceDetector = new CascadeClassifier(mCascadeFile.getAbsolutePath());
            faceDetector.load(mCascadeFile.getAbsolutePath());
            if(faceDetector.empty()) {
                Log.d(TAG, "error with face detector");
            }

            InputStream is2 = context.getResources().openRawResource(R.raw.haarcascade_eye);
            File cascadeDir2 = context.getDir("cascade2", Context.MODE_PRIVATE);
            File mCascadeFile2 = new File(cascadeDir2, "haarcascade_eye.xml");
            FileOutputStream os2 = new FileOutputStream(mCascadeFile2);


            byte[] buffer2 = new byte[4096];
            int bytesRead2;
            while ((bytesRead2 = is2.read(buffer2)) != -1) {
                os2.write(buffer2, 0, bytesRead2);
            }
            is2.close();
            os2.close();


            // Load the cascade classifier
            eyeDetector = new CascadeClassifier(mCascadeFile2.getAbsolutePath());
            eyeDetector.load(mCascadeFile2.getAbsolutePath());
            if(eyeDetector.empty()) {
                Log.d(TAG, "error with eye detector");
            }
            cascadeDir.delete();
            cascadeDir2.delete();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("OpenCVActivity", "Error loading cascade", e);
        }

    }

    public MatOfRect findFaces(Mat frame, float mAbsoluteFaceSize){
        //update frame
        this.frame = frame;
        //alloc faces
        MatOfRect faces = new MatOfRect();

        faceDetector.detectMultiScale(frame, faces,1.1, 2, 2,
                new Size(mAbsoluteFaceSize, mAbsoluteFaceSize), new Size());

        //return populated faces mat
        return faces;
    }

    public MatOfRect findEyes(Mat crop){
        //alloc eyes
        MatOfRect eyes = new MatOfRect();

        //detect eyes
        eyeDetector.detectMultiScale(crop, eyes, 1.1, 2, 2, new Size(30, 30), new Size());

        //return populated eyes mat
        return eyes;
    }

    public Mat findIris(Mat crop){

        //create img references for processing
        Mat source = crop.clone();
        Mat destination = new Mat();
        Mat processed = new Mat();

        //convert source image to gray scale
        Imgproc.cvtColor(source, source, Imgproc.COLOR_RGB2GRAY);

        // Step 1.)Histogram Equalisation to accentuate the dark and light areas
        Imgproc.equalizeHist(source, destination);

        // Step 2.)A binary threshold is applied to reduce the noise and highlight the dark regions
        Imgproc.threshold(destination, processed, 50, 255, Imgproc.THRESH_BINARY_INV);

        CLAHE clahe = Imgproc.createCLAHE(10, new Size(8, 8));
        clahe.apply(processed, processed);

        //Step 3.)The image is then smoothed to further reduce noise and make patterns more apparent
        Imgproc.GaussianBlur(processed, processed, new Size(9, 9), 2, 2);

        //calculate accumulator value
        int accumulatorThreshold = findAccumulatorThreshold(processed);
        //int accumulatorThreshold = 18;

        //the higher the threshold, the less circles you get.

        //just for testing! Can't have neg threshold or houghCircles will have an epic faillll
        assert(accumulatorThreshold > 0);

        // attempt to find circles
        Mat circles = new Mat();
        Imgproc.HoughCircles(processed, circles, Imgproc.HOUGH_GRADIENT, 1, processed.rows() / 8, CANNY_THRESHOLD, accumulatorThreshold, 0, 0);

        return circles;
    }

    public int findAccumulatorThreshold(Mat greyScaleImg){
        //map where key is numb circles, and value is threshold
        HashMap<Integer, Integer> thresholdMap = new HashMap();

        int minNumbCircles = Integer.MAX_VALUE;
        int accumulatorThreshold = -1;
        int low = 1;
        int high = MAX_ACCUMULATOR_THRESHOLD;

        //do binary search for proper threshold
        while ((low < high) && (accumulatorThreshold == -1)) {
            int mid = (low + high) / 2;
            int numbCircles = returnNumbCircles(greyScaleImg, mid);

            if (numbCircles > 1) {
                low = mid + 1;
            } else if (numbCircles == 0) {
                high = mid - 1;
            } else {
                accumulatorThreshold = mid;
            }

            if (numbCircles != 0) {
                //keep track of results in map
                thresholdMap.put(numbCircles, mid);

                //update min numb circles
                if (numbCircles < minNumbCircles) {
                    minNumbCircles = numbCircles;
                }
            }
        }

        //if no threshold value found, return the threshold with min number of circles
        if (accumulatorThreshold == -1) {
            accumulatorThreshold = thresholdMap.get(minNumbCircles);
        }

        return accumulatorThreshold;
    }

    /*
    Return number of circles found in a grey scale image, given a specific accumulator threshold.
    */
    public int returnNumbCircles(Mat inputImgToGrey, int accumulatorThreshold)
    {
        if (accumulatorThreshold < 1) return 0;

        Mat circles = new Mat();
        Imgproc.HoughCircles(inputImgToGrey, circles, Imgproc.HOUGH_GRADIENT, 1, inputImgToGrey.rows() / 8, CANNY_THRESHOLD, accumulatorThreshold, 0, 0);

        //log values for testing

        return (int)circles.total();
    }
}
