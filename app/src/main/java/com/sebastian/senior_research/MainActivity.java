package com.sebastian.senior_research;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceView;
import android.view.WindowManager;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements CameraBridgeViewBase.CvCameraViewListener2{
    static final String TAG = "MAIN ACTIVITY";
    private CameraBridgeViewBase mOpenCvCameraView;
    private CalibrationModel model;
    private Mat matFrame, croppy;
    private MenuItem               mItemFace50;
    private MenuItem               mItemFace40;
    private MenuItem               mItemFace30;
    private MenuItem               mItemFace20;
    private float                  mRelativeFaceSize   = 0.2f;
    private int                    mAbsoluteFaceSize   = 0;
    private ArrayList<Point> leftPoints, rightPoints;
    private Rect    leftEye, rightEye;
    private Point   leftCenter, rightCenter;
    private int     frameCnt, collectionCnt;


    //Radius which effects size of circle drawn for each eye
    private static final int RADIUS_DRAW_SIZE  = 15;
    //eye indices for determining left from right eye
    private static final int LEFT_EYE_INDEX = 0;
    private static final int RIGHT_EYE_INDEX = 1;

    //threshold for determining outlier points
    private static final int THRESHOLD_OFFSET = 10;

    //Rate at which we do averaging for best eye centers
    //best results so far for me == 8
    private static final int FRAME_UPDATE_RATE = 8;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //current frame count
        frameCnt = 0;
        collectionCnt = 0;

        //points we store for averaging on each eye
        leftPoints = new ArrayList();
        rightPoints = new ArrayList();

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.HelloOpenCvView);
        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
        mOpenCvCameraView.setCameraIndex(CameraBridgeViewBase.CAMERA_ID_FRONT);
        mOpenCvCameraView.setCvCameraViewListener(this);
    }

    @Override
    public void onPause()
    {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    public void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.i(TAG, "called onCreateOptionsMenu");
        mItemFace50 = menu.add("Face size 50%");
        mItemFace40 = menu.add("Face size 40%");
        mItemFace30 = menu.add("Face size 30%");
        mItemFace20 = menu.add("Face size 20%");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.i(TAG, "called onOptionsItemSelected; selected item: " + item);
        if (item == mItemFace50)
            setMinFaceSize(0.5f);
        else if (item == mItemFace40)
            setMinFaceSize(0.4f);
        else if (item == mItemFace30)
            setMinFaceSize(0.3f);
        else if (item == mItemFace20)
            setMinFaceSize(0.2f);
        return true;
    }

    private void setMinFaceSize(float faceSize) {
        mRelativeFaceSize = faceSize;
        mAbsoluteFaceSize = 0;
    }

    public void onCameraViewStarted(int width, int height) {
    }

    public void onCameraViewStopped() {
    }

    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        matFrame = inputFrame.rgba();
        if (mAbsoluteFaceSize == 0) {
            int height = matFrame.rows();
            if (Math.round(height * mRelativeFaceSize) > 0) {
                mAbsoluteFaceSize = Math.round(height * mRelativeFaceSize);
            }
        }
        MatOfRect faces = model.findFaces(matFrame, mAbsoluteFaceSize);
        drawFeatures(faces);
        frameCnt++;
        return matFrame;
    }

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i(TAG, "OpenCV loaded successfully");
                    mOpenCvCameraView.enableView();
                    model = new CalibrationModel(MainActivity.this);
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };

    public void drawFeatures(MatOfRect faces)
    {
        for( Rect rect : faces.toArray()){
            //create submat for returned faces from classifier
            Mat face = matFrame.submat(rect);
           // croppy = face.submat(0, (2 * face.width() / 3), 0, face.height());
            if (face.height() > 0 && face.width() > 0) {
                croppy = face.submat((face.height()/4), (2 * face.height() / 4), 0, face.width());
            }
            //find the eyes from the subregion of the face
            MatOfRect eyes = model.findEyes(croppy);

            int eyeIndex = 0;
            for (Rect eyeRect : eyes.toArray()) {
                /**
                 * Check which eye we are using based on its index in eyes array
                 * Note: eye array has 2 entries, the left eye and right eye
                 * The left eye is at index 0, right eye is at index 1
                 */
                // int midpoint = face.width()/2;
                //boolean isLeftEye = (eyeIndex == 0);
                updateEyeValues(eyeIndex, eyeRect);


                /**
                 * After X frames have run, display average of the frames
                 */
                if (frameCnt >= FRAME_UPDATE_RATE) {
                    //get best circle for left points and update value for drawing
                    // System.out.println("------ averaging " + leftPoints.size() + " points for left eye ----------");
                    leftCenter = returnCenter(leftPoints);

                    //get best circle for right points and update value for drawing
                    //System.out.println("------ averaging " + rightPoints.size() + " points for right eye ----------");
                    rightCenter = returnCenter(rightPoints);

                    //writeFiles(faceRect);

                    //reset frame counter
                    frameCnt = 0;

                    //clear arrays
                    rightPoints.clear();
                    leftPoints.clear();
                }

                //increment index value
                eyeIndex++;

            } //end of eyes array loop
            /**
             * NOTE: when moved inside the eye loop, we are able to account for larger head movements
             * However, by moving the drawing to this loop, we achieve a smoother result overall
             * We will have to discuss trade offs
             */
            //draw the eye circle based on the averaged points
            try {
                if (leftCenter != null && leftEye != null) {
                    Imgproc.circle(croppy.submat(leftEye), leftCenter, RADIUS_DRAW_SIZE, new Scalar(0, 255, 0), 1);
                }

                if (rightCenter != null && rightEye != null) {
                    Imgproc.circle(croppy.submat(rightEye), rightCenter, RADIUS_DRAW_SIZE, new Scalar(0, 255, 0), 1);
                }
            }catch (Exception e){
                e.printStackTrace();
            }

//            if (leftCenter != null && rightCenter != null) {
//                Imgproc.circle(cropped.submat(leftEye),  leftCenter,   RADIUS_DRAW_SIZE, new Scalar(0, 255, 0), 1);
//                Imgproc.circle(cropped.submat(rightEye), rightCenter,  RADIUS_DRAW_SIZE, new Scalar(0, 255, 0), 1);
//            }

            //draw rect for face on video frame
            Imgproc.rectangle(matFrame, new Point(rect.x, rect.y),
                    new Point(rect.x + rect.width, rect.y + rect.height),
                    new Scalar(0, 255, 0)
            );
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_1_0, this, mLoaderCallback);
    }

    /**
     * update the eye region location, ie. leftEye and rightEye for drawing
     * also update the points list associated with each eye region
     */
    private void updateEyeValues(int eyeIndex, Rect eyeRect) {
        if (eyeIndex == LEFT_EYE_INDEX) {
            System.out.println("------ working on left eye ----------");
            leftEye = eyeRect;
        } else if (eyeIndex == RIGHT_EYE_INDEX) {
            System.out.println("------ working on right eye ----------");
            rightEye = eyeRect;
        }

        //get potential circles from eye region
        Mat circles = model.findIris(croppy.submat(eyeRect));

        for (int index = 0; index < circles.cols(); index++) {
            //grab circle instance
            double[] circle = circles.get(0, index);

            //get center of circle point, and add to points list for averaging
            Point circleCenter = new Point(Math.round(circle[0]), Math.round(circle[1]));
            //int circleRadius = (int) circle[2];

            if (eyeIndex == LEFT_EYE_INDEX) {
                leftPoints.add(circleCenter);
                // leftRadiusPoints.add(circleRadius);
            } else if (eyeIndex == RIGHT_EYE_INDEX) {
                rightPoints.add(circleCenter);
                //rightRadiusPoints.add(circleRadius);
            }
        }
    }

    /**
     * Method that returns the average point given a set of points
     * @param points = array of X,Y points for potential circles
     * @return average point of all points
     */
    private Point returnCenter(ArrayList<Point> points) {
        if (points.size() < 0)
            return null;

        if (points.size() == 1)
            return points.get(0);

        double x = 0;
        double y = 0;
        for(int i = 0; i < points.size(); i++) {
            x += points.get(i).x;
            y += points.get(i).y;
        }

        Point point = new Point(x/points.size(), y/points.size());
        return point;
    }

    private Integer returnRadius(ArrayList<Integer> points) {
        if (points.size() < 0)
            return null;

        if (points.size() == 1)
            return points.get(0);

        int radius = 0;
        for(int i = 0; i < points.size(); i++) {
            radius += points.get(i);
        }

        radius = radius/points.size();
        return radius;
    }
}
